// Retrieve tasks from local storage or initialize an empty array
let tasks = JSON.parse(localStorage.getItem("tasks")) || [];

// Function to render the tasks on the page
function renderTasks() {
  const taskList = document.getElementById("task-list");
  taskList.innerHTML = "";

  tasks.forEach((task, index) => {
    const listItem = document.createElement("li");
    listItem.className =
      "flex items-center justify-between bg-white rounded-lg shadow p-4 mb-4";

    const taskText = document.createElement("span");
    taskText.className = task.completed ? "line-through" : "";
    taskText.textContent = task.text;

    const deleteBtn = document.createElement("button");
    deleteBtn.className =
      "text-red-500 hover:text-red-700 focus:outline-none";
    deleteBtn.innerHTML = '<i class="fas fa-trash"></i>';
    deleteBtn.addEventListener("click", () => {
      deleteTask(index);
    });

    const completeBtn = document.createElement("button");
    completeBtn.className =
      "text-green-500 hover:text-green-700 focus:outline-none";
    completeBtn.innerHTML = '<i class="fas fa-check"></i>';
    completeBtn.addEventListener("click", () => {
      toggleComplete(index);
    });

    listItem.appendChild(taskText);
    listItem.appendChild(deleteBtn);
    listItem.appendChild(completeBtn);

    taskList.appendChild(listItem);
  });
}

// Function to add a new task
function addTask() {
  const taskInput = document.getElementById("task-input");
  const taskText = taskInput.value.trim();

  if (taskText !== "") {
    const newTask = {
      text: taskText,
      completed: false,
    };

    tasks.push(newTask);
    saveTasks();
    renderTasks();

    taskInput.value = "";
  }
}

// Function to delete a task
function deleteTask(index) {
  tasks.splice(index, 1);
  saveTasks();
  renderTasks();
}

// Function to toggle the completion status of a task
function toggleComplete(index) {
  tasks[index].completed = !tasks[index].completed;
  saveTasks();
  renderTasks();
}

// Function to save tasks to local storage
function saveTasks() {
  localStorage.setItem("tasks", JSON.stringify(tasks));
}

// Event listener for add task button
const addTaskBtn = document.getElementById("add-task-btn");
addTaskBtn.addEventListener("click", addTask);

// Render the initial tasks on page load
renderTasks();
